/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import ufps.util.colecciones_seed.ListaCD;

/**
 *
 * @author madar
 */
public class Semestre {

    private ListaCD<Estudiante> estudiantes;

    public Semestre() {
        this.estudiantes = new ListaCD();
    }

    public void agregarEstudiante(int cod, String nombre, String email) {
        this.estudiantes.insertarFin(new Estudiante(cod, nombre, email));
    }

    @Override
    public String toString() {
        String msg = "";
        for (Estudiante x : this.estudiantes) {
            msg+="\n"+x.toString();
        }
        return msg;
    }

    
    public boolean contieneEstudiantes()
    {
        return this.estudiantes.getTamanio()>0;
    }

    public ListaCD<Estudiante> getEstudiantes() {
        return estudiantes;
    }
    
    
}
