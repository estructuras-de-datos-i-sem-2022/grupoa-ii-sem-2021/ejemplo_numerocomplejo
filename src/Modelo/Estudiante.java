/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Estudiante {

    private int codigo;
    private String nombre;
    private String email;
    private float p1, p2, p3, examen;

    public Estudiante(int codigo, String nombre, String email, float p1, float p2, float p3, float examen) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.email = email;
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.examen = examen;
    }

    public Estudiante() {
    }

    public Estudiante(int codigo, String nombre, String email) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.email = email;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "codigo=" + codigo + ", nombre=" + nombre + ", email=" + email + " Promedio=" + this.getPromedio() + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + this.codigo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estudiante other = (Estudiante) obj;
        if (this.codigo != other.codigo) {
            return false;
        }
        return true;
    }

    public float getP1() {
        return p1;
    }

    public void setP1(float p1) {
        this.p1 = p1;
    }

    public float getP2() {
        return p2;
    }

    public void setP2(float p2) {
        this.p2 = p2;
    }

    public float getP3() {
        return p3;
    }

    public void setP3(float p3) {
        this.p3 = p3;
    }

    public float getExamen() {
        return examen;
    }

    public void setExamen(float examen) {
        this.examen = examen;
    }

    public float getPromedio() {
        return ((this.p1 + this.p2 + this.p3) / 3F) * 0.7F + this.examen * 0.3F;
    }

    public int getPrioridad() {

        return (int) ((this.codigo * 1000 + this.getPromedio())*-1);
    }

}
