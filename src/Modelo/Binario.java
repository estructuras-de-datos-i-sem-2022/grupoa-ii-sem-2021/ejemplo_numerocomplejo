/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import ufps.util.colecciones_seed.ListaCD;

/**
 *          CLASE QUE REPRESENTA UN BINARIO
 *  LOS MÉTODO USAN ITERATOR Y/O LISTITERATOR SEGÚN
 * SEA LA ESTRATEGIA DE SOLUCIÓN PLANTEADA
 * @author madar
 */
public class Binario {
    
    private ListaCD<Bit> bits=new ListaCD();

    public Binario() {
    }
    
    /**
     * Dado un entero positivo lo convierte a binario
     * Ejemplo: 23
     * Ver vídeo: https://www.youtube.com/watch?v=CXKjWbTf9CQ
     * bits<true, false,true,true,true>--> bits.toString()-->10111
     * @param decimal un entero positivo
     */
    public Binario(int decimal) {
    }
    
    /**
     * Obtiene el decimal a partir del número binario (colección de bits)
     * @return un entero
     */
    public int getDecimal()
    {
        return 0;
    }
    
    /**
     * Realiza la suma binaria
     * @param dos un numero binario
     * @return un objeto de la clase Binario con el resultado de la suma
     */
    public Binario getSuma(Binario dos)
    {
        return null;
    }
    
    
    /**
     * Realiza la resta binaria
     * 
     * SUPONGA QUE BINARIO1 > BINARIO2
     * 
     *      SE REALIZA USANDO COMPLEMENTO A 2
     * VER VÍDEO: https://www.youtube.com/watch?v=mG0SuB2XohQ
     * @param dos un numero binario
     * @return un objeto de la clase Binario con el resultado de la resta
     */
    public Binario getResta(Binario dos)
    {
        return null;
    }

    @Override
    public String toString() {
        return "";
    }
    
    
}
