/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.NumeroComplejo;
import util.Secuencia;

/**
 *
 * @author madar
 */
public class Test_Secuencia_Generica {

    public static void main(String[] args) {
        Secuencia<Integer> s = new Secuencia(3);
        Secuencia<String> s2 = new Secuencia(3);
        Secuencia<NumeroComplejo> s3 = new Secuencia(3);

        s.adicionar(0, 12);
        s.adicionar(1, 3);
        s.adicionar(2, 2);

        s2.adicionar(0, "madarme");
        s2.adicionar(1, "anderson");
        s2.adicionar(2, "samuel");

        s3.adicionar(0, new NumeroComplejo(2, 4));
        s3.adicionar(1, new NumeroComplejo(12, 14));
        s3.adicionar(2, new NumeroComplejo(1, 3));

        imprimirSecuencia(s);
        imprimirSecuencia(s2);
        imprimirSecuencia(s3);
    }

    private static <T> void imprimirSecuencia(Secuencia<T> s) {
        String msg = "\n";
        for (int i = 0; i < s.length(); i++) {
            msg += s.getElemento(i).toString() + "\t";

        }
        System.out.println(msg);
    }

}
