/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import ufps.util.colecciones_seed.ColaP;

/**
 *
 * @author madar
 */
public class Test_ColaPrioridad {

    public static void main(String[] args) {
        test1();
        test2();
    }

    private static void test1() {

        Estudiante uno = new Estudiante(115, "samuel", "algo@ufps.edu.co");
        Estudiante dos = new Estudiante(112, "kevin", "algo@ufps.edu.co");
        Estudiante tres = new Estudiante(110, "jeniffer", "algo@ufps.edu.co");
        Estudiante cuatro = new Estudiante(11, "ana", "algo@ufps.edu.co");
        ColaP<Estudiante> cola = new ColaP();
        cola.enColar(uno, uno.getCodigo() * -1);
        cola.enColar(dos, dos.getCodigo() * -1);
        cola.enColar(tres, tres.getCodigo() * -1);
        cola.enColar(cuatro, cuatro.getCodigo() * -1);
        imprimirCola(cola, "Personas con prioridad de código");

    }
    
    private static void test2() {

        Estudiante uno = new Estudiante(115, "samuel", "algo@ufps.edu.co",2,2,2,3);
        Estudiante dos = new Estudiante(109, "kevin", "algo@ufps.edu.co",2,2,2,3);
        Estudiante tres = new Estudiante(110, "jeniffer", "algo@ufps.edu.co",2,2,2,3);
        Estudiante cuatro = new Estudiante(11, "ana", "algo@ufps.edu.co",2,2,2,3);
        ColaP<Estudiante> cola = new ColaP();
        cola.enColar(uno, uno.getPrioridad());
        cola.enColar(dos, dos.getPrioridad());
        cola.enColar(tres, tres.getPrioridad());
        cola.enColar(cuatro, cuatro.getPrioridad());
        imprimirCola(cola, "Personas con prioridad de Promedio");

    }

    
    

    private static void imprimirCola(ColaP cola, String msg) {
        System.out.println(msg);
        while (!cola.esVacia()) {
            System.out.println(cola.deColar().toString());
        }
    }
}
